/**
 * Dark theme for Highcharts JS
 * @author Torstein Honsi
 */
// Set Theme Type (DARK is default)
Highcharts.themetype = true;
// Load the fonts
Highcharts.createElement('link', {
   href: '//fonts.googleapis.com/css?family=Open+Sans',
   rel: 'stylesheet',
   type: 'text/css'
}, null, document.getElementsByTagName('head')[0]);
// Keep a copy of defaults
var HCDefaults = $.extend(true, {}, Highcharts.getOptions(), {});
//Update defaults
    HCDefaults.colors = ["#FF7500", "#434348", "#90ed7d", "#f7a35c", "#8085e9", "#f15c80", "#e4d354", "#8085e8", "#8d4653", "#91e8e1"];
    HCDefaults.xAxis =  {
      // gridLineColor: '#E0E0E3',
       gridLineColor: '#666666',
      labels: {
         style: {
            color: '#888888'
         }
      },
      // lineColor: '#E0E0E3',
      lineColor: '#666666',
      // minorGridLineColor: '#C2C2C4',
       minorGridLineColor: '#666666',
      tickColor: '#E0E0E3',
      title: {
         style: {
            color: '#888888'

         }
      }
   };
   HCDefaults.yAxis = {
      gridLineColor: '#666666',
      labels: {
         style: {
            color: '#888888'
         }
      },
      lineColor: '#666666',
      minorGridLineColor: '#666666',
      tickColor: '#E0E0E3',
      tickWidth: 1,
      title: {
         style: {
            color: '#888888'
         }
      }
   };
function ResetOptions(x) {
   if(x)
      Highcharts.setOptions(Highcharts.theme);
   else
    Highcharts.setOptions(HCDefaults);
   Highcharts.themetype = x;
}

Highcharts.theme = {
   colors: ["rgb(0, 173, 255)","#28D14F","#DDE861", "#FF8812","#aaeeee", "#ff0066", "#eeaaee",
      "#55BF3B" , "#7798BF", "#aaeeee"],
   chart: {
      backgroundColor:
         /*linearGradient: { x1: 0, y1: 0, x2: 1, y2: 1 },
         stops: [
            [0, '#2a2a2b'],
            [1, '#3e3e40']
         ]*/
         '#444444'
      ,
      style: {
         fontFamily: "'Open Sans', sans-serif"
      },
      plotBorderColor: '#606063'
   },
   title: {
      style: {
         color: '#E0E0E3',
         textTransform: 'uppercase',
         fontSize: '20px'
      }
   },
   subtitle: {
      style: {
         color: '#E0E0E3',
         textTransform: 'uppercase'
      }
   },
   xAxis: {
      // gridLineColor: '#707073',
       gridLineColor: '#666666',
      labels: {
         style: {
            color: '#E0E0E3'
         }
      },
      lineColor: '#666666',
      minorGridLineColor: '#666666',
      tickColor: '#707073',
      title: {
         style: {
            color: '#A0A0A3'

         }
      }
   },
   yAxis: {
      gridLineColor: '#666666',
      labels: {
         style: {
            color: '#E0E0E3'
         }
      },
      lineColor: '#666666',
      minorGridLineColor: '#666666',
      tickColor: '#707073',
      tickWidth: 1,
      title: {
         style: {
            color: '#A0A0A3'
         }
      }
   },
   tooltip: {
      backgroundColor: 'rgba(0, 0, 0, 0.85)',
      style: {
         color: '#F0F0F0'
      }
   },
   plotOptions: {
      series: {
         dataLabels: {
            color: '#B0B0B3'
         },
         marker: {
            lineColor: '#333',
            symbol:'circle'
         },
         borderWidth: 0
      },
      boxplot: {
         fillColor: '#505053'
      },
      candlestick: {
         lineColor: 'white'
      },
      errorbar: {
         color: 'white'
      }
   },
   legend: {
      itemStyle: {
         color: '#E0E0E3'
      },
      itemHoverStyle: {
         color: '#FFF'
      },
      itemHiddenStyle: {
         color: '#606063'
      }
   },
   credits: {
      style: {
         color: '#666'
      }
   },
   labels: {
      style: {
         color: '#707073'
      }
   },

   drilldown: {
      activeAxisLabelStyle: {
         color: '#F0F0F3'
      },
      activeDataLabelStyle: {
         color: '#F0F0F3'
      }
   },

   navigation: {
      buttonOptions: {
         symbolStroke: '#DDDDDD',
         theme: {
            fill: '#505053'
         }
      }
   },

   // scroll charts
   rangeSelector: {
      buttonTheme: {
         fill: '#505053',
         stroke: '#000000',
         style: {
            color: '#CCC'
         },
         states: {
            hover: {
               fill: '#707073',
               stroke: '#000000',
               style: {
                  color: 'white'
               }
            },
            select: {
               fill: '#000003',
               stroke: '#000000',
               style: {
                  color: 'white'
               }
            }
         }
      },
      inputBoxBorderColor: '#505053',
      inputStyle: {
         backgroundColor: '#333',
         color: 'silver'
      },
      labelStyle: {
         color: 'silver'
      }
   },

   navigator: {
      handles: {
         backgroundColor: '#666',
         borderColor: '#AAA'
      },
      outlineColor: '#CCC',
      maskFill: 'rgba(255,255,255,0.1)',
      series: {
         color: '#7798BF',
         lineColor: '#A6C7ED'
      },
      xAxis: {
         gridLineColor: '#505053'
      }
   },

   scrollbar: {
      barBackgroundColor: '#808083',
      barBorderColor: '#808083',
      buttonArrowColor: '#CCC',
      buttonBackgroundColor: '#606063',
      buttonBorderColor: '#606063',
      rifleColor: '#FFF',
      trackBackgroundColor: '#404043',
      trackBorderColor: '#404043'
   },

   // special colors for some of the
   legendBackgroundColor: 'rgba(0, 0, 0, 0.5)',
   background2: '#505053',
   dataLabelsColor: '#B0B0B3',
   textColor: '#C0C0C0',
   contrastTextColor: '#F0F0F3',
   maskColor: 'rgba(255,255,255,0.3)'
};
// Apply the theme
Highcharts.setOptions(Highcharts.theme);