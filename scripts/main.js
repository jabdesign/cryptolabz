$(document).on('ready',function(){
  init();
});

function init(){
  // load json
  $.getJSON('data/bitstamp_usdbtc_daily.json',
    function(d){
      var data = d.result.data;
      var payload = [];
      _.each(data,function(d,key){
        payload.push([moment(key).unix()*1000,d[0]]);
      });
      console.log(payload);
      plot({payload:payload});
    });
}

function plot(opt){
  opt = opt || null;
  chart = new Highcharts.Chart({ //Stock
    chart: {
    renderTo: 'container1',
    type:'line',
    zoomType: 'x'
  },
  legend:{
    enabled:false
  },
  navigator: {
    adaptToUpdatedData: false,
      enabled: true,
      height: 15,
      xAxis:{
        labels: {
          enabled: false
        }
      },
    margin:5
  },
  scrollbar: {
    liveRedraw: true,
    enabled:false
  },
  plotOptions: {
    series: {
      lineWidth:1,
      animation:true,
      shadow:false,
      stickyTracking:false,
      marker: {
        enabled: false
      },
      states: {
        hover: {
         enabled: true
        }
      },
      cursor:'pointer',
    }
  },
  title:{
    text:''
  },
  xAxis:  { type:'datetime',minPadding:0.02,maxPadding:0.02,ordinal:false},
  yAxis:  [{opposite:false,title:{text:''}},{opposite:true,title:{text:''}}],
  series:[{data:opt.payload}]
});

}